## A stand-alone container where Athena/AtlasOffline is installed and setup

FROM atlas/slc6-atlasos:latest

ARG TIMESTAMP=""
ARG RELEASE=21.0.15
ARG PROJECT=AtlasOffline
ARG PACKAGE=${PROJECT}_${RELEASE}_x86_64-slc6-gcc62-opt
ARG NIGHTLYVER=21.0/x86_64-slc6-gcc62-opt/${TIMESTAMP}

ENV AtlasProject=$PROJECT

USER root
WORKDIR /root

# Install Atlas packages
COPY repo_config /root/repo_config.sh
RUN if [ ! -z "$TIMESTAMP" ] ; then source /root/repo_config.sh ; fi
RUN yum -y install $PACKAGE --nogpgcheck && \
    yum -y install AtlasSetup libaio --nogpgcheck && \
    yum clean all
    
# Add script to set up path to cmake
COPY cmake_setup /root/cmake_setup.sh

# Add the release setup script to the home directory, and its
# "usage instructions":
COPY release_setup.sh /home/atlas/
COPY release_setup.sh /usr/atlas
COPY motd /etc/

# Add configuration file for steering AtlasSetup
COPY asetup /home/atlas/.asetup
COPY asetup /usr/atlas/.asetup

RUN chown atlas:atlas /home/atlas/.asetup  && \
    chown atlas:atlas /home/atlas/release_setup.sh && \
    echo "export AtlasProject=$AtlasProject" >> /home/atlas/.bashrc && \
    source /root/cmake_setup.sh

CMD cat /etc/motd && su -l atlas
