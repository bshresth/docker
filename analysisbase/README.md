AnalysisBase/AnalysisTop-21.2.X Image Configuration
===================================================

This configuration can be used to build an image providing a
completely standalone installation of an
AnalysisBase/AnalysisTop-21.2.X release.

Building an image for the latest AnalysisBase release should be done
like:

```bash
docker build \
   -t atlas/analysisbase:21.2.83-centos7-20190802 \
   -t atlas/analysisbase:21.2.83-centos7 \
   -t atlas/analysisbase:21.2.83 \
   -t atlas/analysisbase:latest \
   --build-arg BASEIMAGE=atlas/centos7-atlasos:latest \
   --build-arg PROJECT=AnalysisBase \
   --build-arg RELEASE=21.2.83 \
   --build-arg PLATFORM=x86_64-centos7-gcc8-opt \
   --compress --squash .
```

Naturally one should modify these arguments as one would expect to build
AnalysisTop and SLC6 images. The base image for SLC6 releases should be
`BASEIMAGE=atlas/slc6-atlasos:latest`.

You should of course just leave off the "latest tag" from the command
line when building an image for an older release.

Examples
--------

You can find pre-built images on
[atlas/analysisbase](https://hub.docker.com/r/atlas/analysisbase/) and
[atlas/analysistop](https://hub.docker.com/r/atlas/analysistop/).
